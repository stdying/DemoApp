#pragma once
#include <QtQuick/QQuickPaintedItem>

class MyItem2 : public QQuickPaintedItem
{
    Q_OBJECT
    QML_ELEMENT
        Q_DISABLE_COPY(MyItem2)
public:
    explicit MyItem2(QQuickItem *parent = nullptr);
    void paint(QPainter *painter) override;
    ~MyItem2() override;

public slots:
    void onItemClicked();
    void setText(const QString& text);

};
