#include "myitem2.h"

#include <QPainter>

MyItem2::MyItem2(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
}

void MyItem2::paint(QPainter *painter)
{
    QPen pen(QColorConstants::Green, 2);
    QBrush brush(QColorConstants::DarkGreen);

    painter->setPen(pen);
    painter->setBrush(brush);
    painter->drawRect(0, 0, 100, 100);
}

MyItem2::~MyItem2()
{
}

void MyItem2::onItemClicked()
{

}

void MyItem2::setText(const QString& text)
{
    qDebug() << "set " << text;
}
