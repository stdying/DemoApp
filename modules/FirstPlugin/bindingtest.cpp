#include "bindingtest.h"

BindingTest::BindingTest() {
    // test timer to simulate property changes
    QTimer *timer = new QTimer(this);
    timer->setInterval(1000);
    timer->start();
    connect(timer, &QTimer::timeout, this, [&]() {
        static int i = 0;
        setMyVal(10 + i);

        setCounter(100 * i);

        i++;
    });
}
