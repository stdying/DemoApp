#include "myitem.h"

#include <QPainter>

MyItem::MyItem(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{
}

void MyItem::paint(QPainter *painter)
{
    QPen pen(QColorConstants::Red, 2);
    QBrush brush(QColorConstants::Red);

    painter->setPen(pen);
    painter->setBrush(brush);
    painter->drawRect(0, 0, 100, 100);
}

MyItem::~MyItem()
{
}

void MyItem::onItemClicked()
{
    qDebug() << "onItemClicked";
}

void MyItem::setText(const QString& text)
{
    qDebug() << "set " << text;
}
