import QtQuick
import QtQuick.Controls

import Style

Item {
    id: myLabelBase

    // public

    property alias text: myLabel.text
    property alias color: myLabel.color

    property alias background: labelBackground

    // signals

    signal clicked()

    // private

    Label {
        id: myLabel
        padding: 2

        background: Rectangle {
            id: labelBackground
            color: Style.text.color.secondary
        }

        color: Style.text.color.primary

        MouseArea {
            id: myLabelMouseArea
            anchors.fill: myLabel
            onClicked: {
                myLabelBase.clicked();
            }
        }
    }
}
