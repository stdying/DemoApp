#include <QApplication>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QLocale>
#include <QTranslator>

#include <QStandardPaths>
#include <QFile>
#include <QDebug>

#include <QQuickStyle>

#include "enumtest.h"
#include "myquickitem.h"
#include "configfacade.h"
#include "customtablemodel.h"

#include <iostream>

/**
 * @brief Example message handler to handle QML log messages
 */
void myMessageHandler(QtMsgType type, const QMessageLogContext & context, const QString & msg)
{
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("Debug: %1").arg(msg);
        break;
    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
        break;
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
        break;
    default:
        txt = QString("Default: %1").arg(msg);
    }

    std::cout << "MessageHandler: " << qFormatLogMessage(type, context, txt).toStdString() << std::endl;

    //QString qs = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    //qDebug() << "write log to " << qs;

    QFile outFile(/*qs +*/ "MyApplication-log.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << qFormatLogMessage(type, context, txt) << Qt::endl;
}

int main(int argc, char *argv[]) {

    // set a custom message handler to receive log messages from QML and C++
    qInstallMessageHandler(myMessageHandler);

    // change the default message pattern
    qSetMessagePattern("%{appname} %{function}: %{message}");

    // QApplication is needed if we use ChartView, which is based on
    // internally on QWidgets. Otherwise QGuiApplication is sufficient
    QApplication app(argc, argv);

    // TODO choose the style setting
    QQuickStyle::setStyle("Material");

    // TODO set application name
    QString applicationName = "MyApplication";

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        // TODO adapt application name
        const QString baseName = applicationName + "_" + QLocale(locale).name();
        if (translator.load(":/translations/" + baseName)) {
            app.installTranslator(&translator);
            break;
        }
    }

    QQmlApplicationEngine engine;
    engine.addImportPath(":/");

    const QUrl url(QString("qrc:/%1/qml/main.qml").arg(applicationName));
    QObject::connect(
                &engine, &QQmlApplicationEngine::objectCreated, &app,
                [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl) QCoreApplication::exit(-1);
    },
    Qt::QueuedConnection);

    // now we register some stuff to the QML engine to make C++ stuff available in QML

    // register a meta object only containing enums
    qmlRegisterUncreatableMetaObject(
                MyEnums::staticMetaObject,
                "com.test.enums", 1, 0, "MyEnums", "only enums available");

    // add the global styles module. Not sure why this has to be done here
    // explicitly, because we register the module in its qmldir file.
    qmlRegisterSingletonType(QUrl("qrc:///Style/Style.qml"), "Style", 1, 0, "Style");

    // example for registering a C++ item communication with QML
    // the QML_ELEMENT way is currently not working (https://bugreports.qt.io/browse/QTBUG-93443)
    qmlRegisterType<MyQuickItem>("MyQuickItem", 1, 0, "MyQuickItem");
    qmlRegisterType<CustomTableModel>("CustomTableModel", 1, 0, "CustomTableModel");

    // register a singleton to the QML context. This can be used to provide service facades.
    qmlRegisterSingletonInstance("ConfigFacade", 1, 0, "ConfigFacade", ConfigFacade::getInstance());

    engine.load(url);

    return app.exec();
}
