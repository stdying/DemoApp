#include "myquickitem.h"

#include <QPen>
#include <QPainter>
#include <QDebug>

#include "configfacade.h"

MyQuickItem::MyQuickItem() {
    qDebug() << "use config facade from C++: get name" << ConfigFacade::getInstance()->
                getConfigValue("MyQuickItem", "myValue", "Default Value").toString();
}

void MyQuickItem::paint(QPainter *painter) {
    QPen pen(color, 2);
    painter->setPen(pen);
    painter->setRenderHints(QPainter::Antialiasing, true);
    painter->drawPie(boundingRect().adjusted(1, 1, -1, -1), 90 * 16, 230 * 16);
}

void MyQuickItem::onItemClicked(int index) {
    color = QColor(0, 255, 0);
    update();

    emit sendBack(QString("answere from item %1").arg(index));
}
