#include "configfacade.h"

#include <QDebug>
#include <QFile>

#include <QJsonObject>
#include <QJsonArray>

ConfigFacade::ConfigFacade(QObject *parent) : QObject{parent}
{
    // TODO we need some file path handling. This try to store the
    //      file in the execution directory. This might be not allowed.
    loadConfig("config.json");
}

ConfigFacade::~ConfigFacade() {
    qDebug() << "close down config fasade";

    // TODO filename handling
    storeConfig("config.json");
}

QVariant ConfigFacade::getConfigValue(const QString& group, const QString& key, const QString& defaultValue) {
    qDebug() << "ask for " << key << " in group " << group;

    auto configObject = configJSON.object();
    auto section = configObject.value(group);
    QJsonObject sectionObject;
    if (section.isObject()) {
        sectionObject = section.toObject();
    } else {
        QJsonObject tempObject;
        configObject.insert(group, tempObject);
    }

    sectionObject = configObject.value(group).toObject();
    auto value = sectionObject.value(key);
    if (value.isUndefined() && !defaultValue.isEmpty()) {
        // insert default value if given
        sectionObject.insert(key, defaultValue);
        configObject.insert(group, sectionObject);

        configJSON.setObject(configObject);
        return defaultValue;
    }

    return value;
}

bool ConfigFacade::storeConfig(const QString &configFileName) {
    QFile configFile;
    configFile.setFileName(configFileName);
    if (configFile.open(QIODevice::WriteOnly)) {
        configFile.write(configJSON.toJson());
        configFile.close();

        return true;
    }
    return false;
}

bool ConfigFacade::loadConfig(const QString &configFileName) {
    QFile configFile;
    configFile.setFileName(configFileName);
    if (configFile.open(QIODevice::ReadOnly)) {
        qDebug() << "init config fasade from file";
        QByteArray data = configFile.readAll();
        configJSON = QJsonDocument::fromJson(data);
        return true;
    } else {
        qDebug() << "init config fasade from default";
        QJsonObject configObject;
        configJSON.setObject(configObject);

        return false;
    }
}
