#include "customtablemodel.h"

#include <QDebug>

CustomTableModel::CustomTableModel() {
    qDebug() << "init CustomTableModel";

    // TODO somehow get the model data

    for (int row = 0; row < 100; ++row) {
        for (int column = 0; column < 100; ++column) {
            // auto tempIndex = index(row, column);
            auto& rowItem = dataModel[row];
            rowItem[column] = "test";
        }
    }
}

int CustomTableModel::rowCount(const QModelIndex &) const
{
    return 100;
}

int CustomTableModel::columnCount(const QModelIndex &) const
{
    return 25;
}

bool CustomTableModel::setData(const QModelIndex &index, const QVariant &value, int role) {
    qDebug() << "set value: " << value.toString();

    dataModel[index.row()][index.column()] = value.toString().toStdString();

    return true;
}


QVariant CustomTableModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:
    {
        // return QString("%1, %2").arg(index.column()).arg(index.row());
        auto rowItem = dataModel.at(index.row());
        auto value = rowItem.at(index.column());
        return QString::fromStdString(value);
    }
    break;
    case Qt::UserRole:
        return QString("User: %1, %2").arg(index.column()).arg(index.row());
    default:
        break;
    }

    return QVariant();
}

QHash<int, QByteArray> CustomTableModel::roleNames() const
{
    // the role names we use in QML to map it to the display role
    return { {Qt::DisplayRole, "customrole"},
             {Qt::UserRole, "userrole"}  };
}
