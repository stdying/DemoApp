#pragma once

#include <qqml.h>
#include <QObject>
#include <QDebug>
#include <QAbstractTableModel>

class CustomTableModel : public QAbstractTableModel
{
    Q_OBJECT

    std::map<int, std::map<int, std::string>> dataModel;
public:
    CustomTableModel();

    int rowCount(const QModelIndex & = QModelIndex()) const override;

    int columnCount(const QModelIndex & = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role) const override;
    virtual bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::DisplayRole) override;

    QHash<int, QByteArray> roleNames() const override;
};
