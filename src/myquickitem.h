#pragma once

class QPainter;
#include <QQuickPaintedItem>

/**
 * @brief Sample quick item with callback slot and native painting in
 *        the @paint() methods
 *
 *        This demonstrates how to call C++ slots from QML and how to emit
 *        signals from QML to C++
 */
class MyQuickItem : public QQuickPaintedItem {
    Q_OBJECT

    QColor color = QColor(255, 255, 0);
public:
    MyQuickItem();

    /**
     * @brief Example custom painting of an item
     *
     * @param painter
     */
    void paint(QPainter *painter) override;

signals:
    /**
     * @brief Signal used to send back a message to QML
     * @param message   the message send back to the page
     */
    void sendBack(const QString& message);

public slots:
    /**
     * @brief Slot to react on item clicks in QML
     *
     * @param index the index of the clicked item
     */
    void onItemClicked(int index);
};
