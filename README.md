# DemoApp

## General

This is a QML test application based on CMAKE. It addresses several general question arising when you start with QML.
Focus is on structuring the application into meaningful parts to be flexible and extendible.

## Features

Following features are available:

- [ ] CMAKE file (solving project structuring issues, source grouping etc.)
- [ ] QML main window bases on StackView with menu, toolbar, drawer with page navigation
- [ ] Separated Plugin structure realised with Modules
- [ ] Examples for registering classes, bridging C++ <--> QML
- [ ] Basic QQuickPaintedItem implementation, QML Controls usage etc.
- [ ] Global Styling via QML file
- [ ] Singleton config facade implementation for C++ and QML (using JSON files)
- [ ] Example C++ table model implementation used in QML
- [ ] Translation and Resources usage basics
- [ ] Testing basics for C++ an QML test
- [ ] CMAKE configuration file (for e.g. clean builds without test stuff)


and other convinience functions to ease the implementation start of a new QML application.

Check the comments in the files to get more informations about the parts.
Check TODOs to adapt the demo to your needs.

## License

There is no license. Just use it as you wish...