import QtQuick

import QtQuick.Controls
import QtQuick.Dialogs

import Style

// Example item defining a user QML component publishing some properties, react on mouse clicks to show MessageDialog
Item {
    id: myComponent

    width: 500; height: 100

    property color componentColor: Style.text.color.secondary
    property alias componentBackground: myRectangle.color

    signal componentClicked()

    Rectangle {
        id: myRectangle

        width: myComponent.width
        height: myComponent.height

        Text {
            id: myText
            color:  Qt.darker(myComponent.componentColor)

            width: myRectangle.width
            height: myRectangle.height

            anchors.centerIn: myRectangle
            text: qsTr("My Component", "Comment for translator")

            // example mouse area only accepting right clicks.
            MouseArea {
                cursorShape: Qt.IBeamCursor; acceptedButtons: Qt.RightButton
                anchors.fill: myText
                onClicked: (mouse) => {
                               console.log("X" + mouse.x);
                               myComponent.componentClicked();
                               contextMenu.popup();
                           }
            }

            Menu {
                id: contextMenu
                MenuItem {
                    text: "Show Dialog"
                    onClicked: {
                        console.log("Show Dialog clicked");
                        messageDialog.visible = true;
                    }
                }
            }

        }
    }

    MessageDialog {
        id: messageDialog
        title: "Dialog title"
        text: "This is a dialog"
        onAccepted: {
            console.log("OK clicked.")
        }

        buttons: MessageDialog.Ok | MessageDialog.Cancel
    }

}
