import QtQuick
import QtQuick.Controls
import QtQuick.Layouts

import SecondPlugin
import FirstPlugin
import Style

// alias include
import "components" as Components

// Page with some external items defined in seperate modules placed in a GridLayout
Page {
    id: page3
    title: qsTr("Page 3")

    // expand to full width of main window
    width: window.width

    Label {
        id: myLabel
        text: qsTr("Page Three")
    }

    GridLayout {
        id: myGrid1

        anchors.fill: parent
        columns: 2
        columnSpacing: 0; rowSpacing: 0

        anchors.top: myLabel.bottom

        // QML type defined in the "components" subfolder
        Components.CustomItem1 {
            id: custom1
            componentBackground: Style.background.color.secondary
            width: 400; height: 100
        }

        // Grid placeholder variant showing nothing
        Rectangle {
            id: test
            width: 1; height: 1
        }

        // QQuickPaintedItem defined in module SecondPlugin
        MyItem2 {
            id: item2
            Layout.alignment: Qt.AlignHCenter

            width: 100; height: 100
            Label {
                text: "My Item 2"; font.bold: true; font.strikeout: true
            }
        }

        // QML type defined in module FirstPlugin
        MyItemControls {
            id: control1
            componentBackground: Style.background.color.primary

            // attached properties guiding resizing
            Layout.fillHeight: true
            Layout.fillWidth: true
            Layout.maximumHeight: 300
        }
    }
}
